# Setup

The basic system was set up with:

```
npm create vite@latest sandbox-vue -- --template vue
cd sandbox-vue
npm install
```

# Pinia Sandbox

```
npm install -s pinia
```

Goal here is to have two stores and some basic components that interact with them to find the ideal way to set this up in isolation from other complexities.  Plan:

1. Instruments store, list of instruments fetched from a fake async endpoint
2. Instrument display/edit somehow (nested store or not, still deciding)
3. Research best practices on property passing vs using the store directly
