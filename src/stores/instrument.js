import { defineStore } from 'pinia'

// This isn't necessary yet, not using it, but want to explore still
export const useInstrumentStore = defineStore('instrument', {

    state: () => ({
        name: null,
        id: null,
        wikis: [],
        schema: null
    }),

    getters: {
    },

    actions: {
    },
});
