import { defineStore } from 'pinia'

// pinia uses this behind the scenes, translating the "setup" store
//   into state, getters, and actions, so it seems more direct to do this
export const useInstrumentsStore = defineStore('instruments', {

    state: () => ({
        instruments: [],
        instrument: null,
        wikis: [],
    }),

    getters: {
        count: (state) => state.instruments.length,
    },

    actions: {
        async refreshInstruments() {
            const response = await fetch('/instruments.json')
            const fetched = await response.json();
            this.instruments = fetched.instruments;
        },
        async refreshWikis() {
            const response = await fetch('/wikis.json')
            const fetched = await response.json();
            this.wikis = fetched.wikis;
        },
        select(instrument) {
            this.instrument = instrument;
        },
    },
});
